import string
import random
import re
from unicodedata import name
characters = list(string.ascii_letters + string.digits + "!@#$%^&*()")
class StopLoop(Exception): pass
password = ""
key = "hello" # change system password here
spacing = '-'*6

'''
my program is a little scuffed uhhhhhhhhhhhhh it probs could have worked btr but im lazy :Db
also half of this is done on a caffeine high so uhh if anything iS off or if anything doesnt make sense and i like to use the long way for some stuff so...
but it works!! (hopefully...)
'''

# functions
def random_password():

    # generates random password
    p_len = int(input("Enter password length: "))
    random.shuffle(characters)
    password = []
    for i in range(p_len):
        password.append(random.choice(characters))
    random.shuffle(password)
    password = "".join(password)
    return password

def open_file():

    filepath = 'password.txt'
    with open(filepath) as fp: #add while loop
        line = fp.readlines() # gives the whole chunk of text as a list
        big_dict = {}

        for i in line: # reading the line list one by one, getting the password and username
            lst = re.split('\s+',i)
            # appending stuff to list the var names are so bad im sorry...
            name_index = lst.index("Username")
            psw_index = lst.index("Password")

            # nested dict so that its easier to access items with the platform as the key and the details as the value
            if lst[0] in big_dict:
                big_dict[f"{lst[0]}"]["Username"].append(lst[name_index + 1])
                big_dict[f"{lst[0]}"]["Password"].append(lst[psw_index + 1])

            else:
                big_dict[f"{lst[0]}"] = {"Username": [lst[name_index + 1]], "Password": [lst[psw_index + 1]]}

    return big_dict

def check_platform(): # add validation and loop <3

    platform = input("Enter platform: ").upper()
    try:
        while True:
            if platform in big_dict:
                y_n = input(f"The platform is already saved\n Would you like to continue? (Y/N) ")

                if y_n.upper() == "Y":
                    pass
                elif y_n.upper() == "N":
                    options()

            else:
                print("Platform not inside")

            return platform

    except StopLoop:
        pass

def check_user():

    user = input("Enter username: ")
    return user

def to_string(lst):
    x = ""

    return x.join(lst)

# options

def options():

    options = input("\n 1. Create password\n 2. Retrieve password\n 3. Update username\n 4. Update password\n 5. Delete password\n 6. Exit\n Enter option: ")
    {
    '1':  option1,
    '2':  option2,
    '3':  option3, 
    '4':  option4, 
    '5':  option5,
    '6':  option6, 
    }.get(options, invalid)()
    '''
    if none of the options is chosen, the invalid function will be called instead
    ^ basically an if else replacement, if the input is 1, it will call for function "option1"
    invalid() function is used for error handling, in case the user enters something thats not 1-5, it will inform the user that their input was invalid
    '''

def option1():

    # create password
    platform = check_platform()
    open_file = open("password.txt", "a")
    user = check_user()
    try:
        if user in big_dict[platform]["Username"]:
            print(f"The username {user} is already saved, please try again.")
            options()

    except:
        Exception

    random_yn = input("\nRandom password? (Y/N): ")

    if random_yn.upper() == "Y":
        password = random_password()

    elif random_yn.upper() == "N":
        password = input("Enter password: ")

    else:
        print("Invalid input, please try again") # add loop

    try:
        open_file.write(f"{platform} : Username {user} , Password {password}\n")
        open_file.close()
        print(f"Added \"Username: {user}, Password {password}\" to {platform.lower().capitalize()}")

    except:
        Exception

def option2():

    # retrieve psw
    while True:
        platform = platform = input("Enter platform: ").upper()

        if platform in big_dict:
            print("Platform found, enter username")

            while True:
                user = check_user()

                if user in big_dict[platform]["Username"]:

                    no = big_dict[platform]["Username"].index(f"{user}")
                    psw = big_dict[platform]["Password"][no]

                    print(f"\n{spacing}\nPlatform: {platform.lower().capitalize()}\nUsername: {user}\nPassword: {psw}\n{spacing}\n")
                    break
                else:
                    print("Username not found, please try again.")
        else:
            print("Platform not found, please try again.")
        break

def option3():

    # update user
    while True:
        platform = platform = input("Enter platform: ").upper()

        if platform in big_dict:
            print("Platform found, enter username")

            while True:
                user = check_user()

                if user in big_dict[platform]["Username"]:

                    no = big_dict[platform]["Username"].index(f"{user}")
                    psw = big_dict[platform]["Password"][no]

                    new_user = input("Enter new username: ")
                    big_dict[platform]["Username"][no] = new_user

                    count = 0
                    lst = []
                    filepath = 'password.txt'

                    with open(filepath) as fp:
                        line = fp.readlines()
                        for i in line:
                            lst = re.split('\s+',i)
                            name_index = lst.index("Username")
                            lst.append(lst[name_index + 1])
                            to_string(lst)
                            print(lst)

                    break

                else:
                    print("Username not found, please try again.")

        else:
            print("Platform not found, please try again.")
        break

def option4():
    # update psw

    while True:
        platform = platform = input("Enter platform: ").upper()

        if platform in big_dict:
            print("Platform found, enter username")

            while True:
                user = check_user()

                if user in big_dict[platform]["Username"]:

                    no = big_dict[platform]["Username"].index(f"{user}")
                    psw = big_dict[platform]["Password"][no]

                    
                    break

                else:
                    print("Username not found, please try again.")

        else:
            print("Platform not found, please try again.")

def option5():
    # delete psw
    pass

def option6():
    # exit
    raise StopLoop

def invalid():
    print("Invalid input. Please try again")

while True:
    key_inpt = input("Enter system password: ") # optional, for security reasons (shh its just there so it seems secure ik its not actually secure...)
    if key_inpt == key:
        try:
            while True:
                big_dict = open_file()
                options()
        except StopLoop:
            pass
        break
    else:
        print("Password is wrong, please try again")
